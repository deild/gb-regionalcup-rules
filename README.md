# Guild Ball Regional Cup

Ce dépôt concerne l’effort de traduction en français du [Guild Ball Regional Cup Rules](https://steamforged.com/s/GB-S4-RegionalCup-Rules-190812.pdf).

La dernière version est compilée et proposée [ici](https://gb-regionalcup-rules.netlify.app/).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

All that’s needed is Ruby 2.3 or better and a few Ruby gems.

To check if you have Ruby available, use the `ruby` command to query the version installed:

```sh
ruby --version
```

Make sure this command reports a Ruby version that’s at least 2.3. If so, you may proceed.

### Installing

You can get Asciidoctor by installing the published gem.

Open a terminal and type:

```sh
gem install asciidoctor
```

If you want to install a pre-release version (e.g., a release candidate), use:

```sh
gem install asciidoctor --pre
```

You can get Asciidoctor PDF by installing the published gem. Asciidoctor PDF is published as a pre-release on RubyGems.org.

```sh
gem install asciidoctor-pdf --pre
```

### build html and pdf

```sh
asciidoctor GB-S4-RegionalCup-Rules-FR.asciidoc
asciidoctor -r asciidoctor-pdf -b pdf GB-S4-RegionalCup-Rules-FR.asciidoc
```

## Edit and built With

- [Vim](https://www.vim.org/) - The text editor used
- [vim-asciidoc](https://github.com/asciidoc/vim-asciidoc) - syntax highlighting and filetype plugins for asciidoc
- [asciidoctor](https://asciidoctor.org/) - The text processor used
- [asciidoctor-pdf](https://asciidoctor.org/docs/asciidoctor-pdf/) - The pdf converter used
- [Tufte CSS](https://edwardtufte.github.io/tufte-css/) - Tufte CSS provides tools to style web articles using the ideas demonstrated by Edward Tufte’s books and handouts.

## Contributing

Please read [CONTRIBUTING](https://bit.ly/deild_contributing_en) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/deild/gb-regionalcup-rules/downloads/?tab=tags).

## Authors

- **smur** - _Other works_ - [smur](https://bitbucket.org/deild/)

See also the list of [contributors](contributors.md) who participated in this project.

## License

This project is licensed - see the [LICENSE](LICENSE) file for details
