#!/usr/bin/env bash

# Produce PDFs from all asciidoc files in a directory

# List files to be made by finding all *.asciidoc files and appending .pdf
PDFS := $(patsubst %.asciidoc,%.pdf,$(wildcard *.asciidoc))
# List files to be made by finding all *.asciidoc files and appending .html
HTMLS := $(patsubst %.asciidoc,%.html,$(wildcard *.asciidoc))

# The all rule makes all the PDF and HTML files listed
.PHONY: all
all: $(PDFS) $(HTMLS)

# This generic rule accepts PDF targets with corresponding Asciidoc
# source, and makes them using asciidoctor-pdf
%.pdf: %.asciidoc
	asciidoctor -r asciidoctor-pdf -b pdf $< -D public -o $@

# This generic rule accepts HTML targets with corresponding Asciidoc
# source, and makes them using asciidoctor
%.html: %.asciidoc
	asciidoctor $< -D public -o index.html
#	cp -R et-book/ public/

.PHONY: clean
clean: ## Remove all PDF and HTML outputs
	rm -rf public

build: clean all ## Remove all PDF outputs then build them again

publish: build ## Publish public dir to https://gb-regionalcup-rules.netlify.app/
	netlify deploy --dir=public --prod

.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := build
