# Contributors

![smur's avatar](https://framagit.org/uploads/-/system/user/avatar/42128/avatar.png)

[**Samuel Marcaille**](https://bitbucket.org/smur)

[💻](https://bitbucket.org/deild/gb-regionalcup-rules/commits/ "Code") [🎨](https://bitbucket.org/deild/gb-regionalcup-rules/ "Design") [📖](https://bitbucket.org/deild/gb-regionalcup-rules/src/main/README.md "Documentation")
